import { Component, ViewChild, OnInit } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { NoticiasService } from 'src/app/services/noticias.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page implements OnInit {

  @ViewChild(IonSegment, {static:true}) segment: IonSegment;

  categorias = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];

  categoriaActual = this.categorias[0];

  spinner;

  noticias: Article[] = [];
  

  constructor( private noticiasService: NoticiasService ) {}

  cambiarCategoria(event){
   // debugger;
    console.log(event.currentTarget.value);
    this.categoriaActual = event.currentTarget.value;

    this.noticias = [];

    this.cargarNoticias(event.currentTarget.value);

    if (this.spinner) {
      this.spinner.disabled = false;

    }
    
  }

  ngOnInit() {
    
    this.segment.value = this.categorias[0];

    this.cargarNoticias(this.categorias[0]);
  }

  cargarNoticias( categoria:string, event? ){
    this.noticiasService.getTopHeadlinesCategoria( categoria )
      .subscribe( resp => {
        console.log(resp);
        this.noticias.push(...resp.articles);

        if (resp.articles.length === 0) {
          event.target.disabled = true;
          return;
        }

        if ( event ) {
          this.spinner = event.target;
          event.target.complete();
        }
      });
  }

  loadData( event ) {
    console.log("load data");
    this.cargarNoticias(this.categoriaActual, event );
  }

}
