import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article[] = [];

  constructor( private storage: Storage) { 
    this.cargarFavoritos();
  }

  guardarNoticia( noticia: Article ) {

    const existe = this.noticias.find( noti => noti.title === noticia.title );

    if ( !existe ) {
      this.noticias.unshift( noticia );
  
      this.storage.set('favoritos', this.noticias );
    } else {
      console.log("La noticia ya está en favoritos");
    }


  }

  async cargarFavoritos() {
    /*this.storage.get('favoritos').then( favoritos=> {
      console.log('favoritos', favoritos);
    });*/

    const favoritos = await this.storage.get('favoritos');

    if (favoritos) {
      this.noticias = favoritos;
    } 

  }
}
