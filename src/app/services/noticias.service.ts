import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-key': apiKey
});

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  headlinesPage = 0;

  categoriaActual = '';
  categoriaPage = 0;

  constructor( private http: HttpClient) { }

  private ejecutarQuery( query:string ) {

    query = apiUrl + query;

    return this.http.get<RespuestaTopHeadlines>( query, { headers } );

  }

  getTopHeadlines(){

    this.headlinesPage++;

    return this.ejecutarQuery(`/top-headlines?country=us&page=${ this.headlinesPage }`);

    //return this.http.get<RespuestaTopHeadlines>(`https://newsapi.org/v2/top-headlines?country=us&apiKey=5aeafd1a17c44317bf67daf90fe80665`);
  }

  getTopHeadlinesCategoria( categoria: string) {

    if ( this.categoriaActual === categoria) {
      this.categoriaPage++;
    } else {
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }

    return this.ejecutarQuery(`/top-headlines?country=us&category=${ categoria }&page=${ this.categoriaPage }`);

    //return this.http.get<RespuestaTopHeadlines>(`https://newsapi.org/v2/top-headlines?country=de&category=business&apiKey=5aeafd1a17c44317bf67daf90fe80665`);
  }
}
